/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios.managedbean;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import usuarios.model.Usuario;
import usuarios.utils.StringUtil;

/**
 *
 * @author R3oLoN
 */
@Named(value = "usuarioMB")
@ViewScoped
public class UsuarioMB {

    private static final SelectItem[] ESTADOS = new SelectItem[]{new SelectItem(null, " –- Selecione um Estado -– "),
        new SelectItem("SC", "Santa Catarina"), new SelectItem("PR", "Paraná "), new SelectItem("RS", "Rio Grande do Sul")};
    private static final SelectItem[] CIDADES_PR = new SelectItem[]{new SelectItem(null, "–Selecione uma Cidade–"),
        new SelectItem("CA", "Cascavel"), new SelectItem("CU", "Curitiba"), new SelectItem("FO", "Foz do Iguaçu")};
    private static final SelectItem[] CIDADES_RS = new SelectItem[]{new SelectItem(null, "–Selecione uma Cidade–"),
        new SelectItem("PA", "Porto Alegre"), new SelectItem("CS", "Caxias do Sul"), new SelectItem("PE", "Pelotas")};
    private static final SelectItem[] CIDADES_SC = new SelectItem[]{new SelectItem(null, "–Selecione uma Cidade–"),
        new SelectItem("FL", "Florianópolis"), new SelectItem("CR", "Criciúma"), new SelectItem("BL", "Blumenau")};

    private Usuario usuario;

    /**
     * Creates a new instance of UsuarioMB
     */
    public UsuarioMB() {
    }

    @PostConstruct
    private void initial() {
        usuario = new Usuario();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public SelectItem[] getEstados() {
        return UsuarioMB.ESTADOS;
    }

    public SelectItem[] getCidades() {
        if ("SC".equals(usuario.getEstado())) {
            return UsuarioMB.CIDADES_SC;
        } else if ("PR".equals(usuario.getEstado())) {
            return UsuarioMB.CIDADES_PR;
        } else if ("RS".equals(usuario.getEstado())) {
            return UsuarioMB.CIDADES_RS;
        }
        return null;
    }

    public void valueChangeListener(ValueChangeEvent event) {
        if (event.getNewValue() == null || event.getNewValue().equals(event.getOldValue()) || StringUtil.isEmpity(event.getComponent().getId())) {
            return;
        }
    }
}
